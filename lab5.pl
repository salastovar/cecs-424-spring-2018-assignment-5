% The possible ufo explainations
ufo(ballon).
ufo(clothesline).
ufo(frisbee).
ufo(watertower).

% Days that people saw ufos on
day(tuesday).
day(wednesday).
day(thursday).
day(friday).

% Define what is earlier
earlier(tuesday, wednesday).
earlier(wednesday, thursday).
earlier(thursday, friday).

solve :-
% UFOs that people saw
    ufo(BarradaUFO),
    ufo(GortUFO),
    ufo(KlatuUFO),
    ufo(NiktoUFO),

% People saw different ufos
    all_different([BarradaUFO, GortUFO, KlatuUFO, NiktoUFO]),

% The day people saw the ufo
    day(BarradaDay),
    day(GortDay),
    day(KlatuDay),
    day(NiktoDay),

% People saw ufo in a different day
    all_different([BarradaDay, GortDay, KlatuDay, NiktoDay]),

    Triples = [[barrada, BarradaUFO, BarradaDay],
              [gort, GortUFO, GortDay],
              [klatu, KlatuUFO, KlatuDay],
              [nikto, NiktoUFO, NiktoDay]
    ],

% 1. Mr Klatu made his sighting at some point earlier
%    in the week than the one who saw the ballon, but
%    at some point later in the week than the one who
%    spotted the Frisbee (Who isnt Ms. Gort)

    % Klatu saw something on day X
     member([klatu,_, X], Triples),
     
    % Someone saw the ballon on day Y 
     member([_, ballon,Y],Triples),

    % Not gort who spotted the frisbee
     \+(member([gort, frisbee, _], Triples)),

    % Someone saw the frisbee on day Z
     member([_,frisbee,Z], Triples),

    % X was earlier than the one who saw the ballon
    earlier(X,Y),

    % X is later than the frisbee
    earlier(Z,X),


% 2. Fridays sighting was made by either Ms. Barrada or the one who saw
% a clothesline, or both

    % Barrada saw  _________ on friday
    (member([barrada, _, friday], Triples);

    % _______ saw a clothesline on friday
     member([_, clothesline, friday], Triples);

    % or both
     member([barrada, clothesline, friday], Triples)),

    % 3. Mr. Nikto did not make his sighting on Tuesday

    % nikto did not see ______ on tuesday
    \+(member([nikto, _, tuesday], Triples)),

    % 4. Mr. Klatu isnt the one whose object turned out to be a water tower

    % klatu did not see a water tower on _________.
    \+(member([klatu, watertower, _], Triples)),

    tell(barrada, BarradaUFO, BarradaDay),
    tell(gort, GortUFO, GortDay),
    tell(klatu, KlatuUFO, KlatuDay),
    tell(nikto, NiktoUFO, NiktoDay).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.        % (1)
all_different([_ | T]) :- all_different(T).             % (2)
all_different([_]).                                     % (3)

% X is the person, Y is the ufo, and Z is the day.
tell(X, Y, Z) :-
    write(X), write(' saw the '), write(Y), write(' on '), write(Z), write('.'), nl.
